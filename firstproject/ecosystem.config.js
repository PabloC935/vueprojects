module.exports = {
  apps : [
    {
      name: 'the-weather-today',
      port: 3000,
      exec_mode: 'cluster',
      script: './node_modules/nuxt-start/bin/nuxt-start.js',
      cwd: '/home/ubuntu/vueprojects/firstproject',
      env: {
        NODE_ENV: 'production'
      }
    }
  ]
};
